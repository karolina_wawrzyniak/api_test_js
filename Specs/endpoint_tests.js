const expect = require('chai').expect;
const request = require("supertest")("https://api.stlouisfed.org/fred/releases?api_key=0cd6c5df7fa3fd2c18762b26b1a5a6a7&file_type=json");

describe("endpoint tests", ()=>{

    describe("Response test", function () {
    it("should have status code 200", async function () {
        const response = await request.get("");
        expect(response.status).to.eql(200)

});

    describe('check offset attribute', () => {
    it('should return non-negative integer for "offset" attribute ', async function () {
        const response = await request.get("");
        expect(response.body).to.have.property('offset').that.is.a('number');
        expect(response.body.offset).to.be.at.least(0); 
        console.log(response.body);
        });

    describe('check realtime_start', () => {
    it('should return "realtime_start" attribute in YYYY-MM-DD format', async function () {
        const response = await request.get("");
        expect(response.body).to.have.property('realtime_start');
        expect(response.body.realtime_start).to.match(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/);
        
    describe('check realtime_start', () => {     
    it('should return "realtime_end" attribute in YYYY-MM-DD format', async function () {
        const response = await request.get("");
        expect(response.body).to.have.property('realtime_end');
        expect(response.body.realtime_end).to.match(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/);
        });
    });
});
});
});
});
})